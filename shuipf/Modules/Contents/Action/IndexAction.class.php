<?php

/**
 * Some rights reserved：abc3210.com
 * Contact email:admin@abc3210.com
 */
class IndexAction extends BaseAction {

    private $url;

    //初始化
    protected function _initialize() {
        parent::_initialize();
        import('Url');
        $this->url = get_instance_of('Url');        
    }

    //首页
    public function index() {
        
        $this->commonTitle();
                               
                
        //获取轮播图
        $circle_images = M('article')->where("catid=24")->field('catid, title,thumb')->select();
      
        foreach ($circle_images as $key => $value) {
            if ($key%2==0) {
                $last_assign[$key][]=$value;
                $last_assign[$key][]=$circle_images[$key+1];
            }        	
        }

        
        $bg_images = M('article')->where("catid=34")->field('catid, title,thumb')->select();
        $bottom_images = M('article')->where("catid=25")->field('catid, title,thumb')->select();
        
        $page = isset($_GET[C("VAR_PAGE")]) ? $_GET[C("VAR_PAGE")] : 1;
        $page = max($page, 1);
        //模板处理
        $tp = explode(".", CONFIG_INDEXTP);
        $template = parseTemplateFile("Index:" . $tp[0]);
         //生成路径
        $urls = $this->url->index($page);
        $GLOBALS['URLRULE'] = $urls['page'];
        
        $news = M('article')->where("catid=36")->field('id, inputtime, title,url')->limit(6)->order('id desc')->select();

        $this->assign('news', $news);
        //轮播图
        $this->assign('circleImage', $last_assign);
    
        $this->assign('bgImage', $bg_images);
        $this->assign('btImage', $bottom_images);
  
        //把分页分配到模板
        $this->assign(C("VAR_PAGE"), $page);
   

        $this->display("Index:" . $tp[0]);
    }

    //栏目列表 
    public function lists() {
        
        //栏目ID
        $catid = I('get.catid', 0, 'intval');
       // echo"<meta charset='utf-8'><pre>";
      //  $_GET[C("VAR_PAGE")]=6 ;        
        //分页
        $page = isset($_GET[C("VAR_PAGE")]) ? $_GET[C("VAR_PAGE")] : 1;
   
        //获取栏目数据
        $category = getCategory($catid);
  
        if (empty($category)) {
            send_http_status(404);
            exit;
        }
        //栏目扩展配置信息
        $setting = $category['setting'];
        //检查是否禁止访问动态页
        if ($setting['listoffmoving']) {
            send_http_status(404);
            exit;
        }
        //生成静态分页数
        $repagenum = (int) $setting['repagenum'];
        if ($repagenum && !$GLOBALS['dynamicRules']) {
            //设置动态访问规则给page分页使用
            $GLOBALS['Rule_Static_Size'] = $repagenum;
            $GLOBALS['dynamicRules'] = CONFIG_SITEURL_MODEL . "index.php?a=lists&catid={$catid}&page=*";
        }
        //父目录
        $parentdir = $category['parentdir'];
        //目录
        $catdir = $category['catdir'];
        //生成路径
        $category_url = $this->url->category_url($catid, $page);
    
        //取得URL规则
        $urls = $category_url['page'];
        //一级菜单
        if ($category['type'] == 0) {
            ///index.php?a=shows&catid=16&id=7
           
            //查询美食中心(固定ID)
           $food_id = M('category')->where("catid=17")->field('arrchildid')->find();
           $id_arr = explode(",", $food_id['arrchildid']);
     
            if (in_array($catid, $id_arr)) {
                $list = M('category')->where("parentid={$category['parentid']}")->select();
               
                if($catid==17) {
                    $first = M('category')->where("parentid={$catid}")->order('catid desc')->find();
                   $this->redirect("/index.php?a=lists&catid={$first['catid']}");
                }
            }else{
                     
                $first = M('article')->where("catid={$catid}")->order('id asc')->find();
                $this->redirect("/index.php?a=shows&catid={$catid}&id={$first['id']}");
  
         }
                 
            //栏目首页模板
            $template = $setting['category_template'] ? $setting['category_template'] : 'category';
            //栏目列表页模板
            $template_list = $setting['list_template'] ? $setting['list_template'] : 'list';
            //判断使用模板类型，如果有子栏目使用频道页模板，终极栏目使用的是列表模板
            $template = $category['child'] ? "Category:" . $template : "List:" . $template_list;
            //去除后缀开始
            $tpar = explode(".", $template, 2);
            //去除完后缀的模板
            $template = $tpar[0];
            unset($tpar);
            $GLOBALS['URLRULE'] = $urls;
                     
        } else if ($category['type'] == 1) {//单页
           
            $db = D('Page');
            $template = $setting['page_template'] ? $setting['page_template'] : 'page';
            //判断使用模板类型，如果有子栏目使用频道页模板，终极栏目使用的是列表模板
            $template = "Page:" . $template;
            //去除后缀开始
            $tpar = explode(".", $template, 2);
            //去除完后缀的模板
            $template = $tpar[0];
            unset($tpar);
            $GLOBALS['URLRULE'] = $urls;
            
            $list = M('category')->where("parentid={$category['parentid']}")->select();
            
            $info = $db->getPage($catid);
            $this->assign($category['setting']['extend']);
            $this->assign($info);
        }
      
      
 
        $this->commonTitle();
        //把分页分配到模板
        $this->assign(C("VAR_PAGE"), $page);
        $this->assign("childcate", $list);
        //分配变量到模板 
        $this->assign($category);

        //seo分配到模板
        $seo = seo($catid, $setting['meta_title'], $setting['meta_description'], $setting['meta_keywords']);
        $this->assign("SEO", $seo);

        $this->display($template);
    }

    //内容页 
    public function shows() {
               
        $catid = I('get.catid', 0, 'intval');
        $id = I('get.id', 0, 'intval');
        $page = intval($_GET[C("VAR_PAGE")]);
        $page = max($page, 1);
        //获取当前栏目数据
        $category = getCategory($catid);
        if (empty($category)) {
            send_http_status(404);
            exit;
        }
        //反序列化栏目配置
        $category['setting'] = $category['setting'];
        //检查是否禁止访问动态页
        if ($category['setting']['showoffmoving']) {
            send_http_status(404);
            exit;
        }
        //模型ID
        $this->modelid = $category['modelid'];
        $this->db = ContentModel::getInstance($this->modelid);
        $data = $this->db->relation(true)->where(array("id" => $id, 'status' => 99))->find();
    
        if (empty($data)) {
            send_http_status(404);
            exit;
        }
        $this->db->dataMerger($data);

        //载入字段数据处理类
        require_cache(RUNTIME_PATH . 'content_output.class.php');
        //tag
        tag('html_shwo_buildhtml', $data);
        $content_output = new content_output($this->modelid);
        //获取字段类型处理以后的数据
        $output_data = $content_output->get($data);
        $output_data['id'] = $id;
        $output_data['title'] = strip_tags($output_data['title']);

        //内容页模板
        $template = $output_data['template'] ? $output_data['template'] : $category['setting']['show_template'];
        //去除模板文件后缀
        $newstempid = explode(".", $template);
        $template = $newstempid[0];
        unset($newstempid);

        $titleList = $this->getArticleTitle($catid);
         
        //分配解析后的文章数据到模板 
        $this->assign($output_data);
            
        $this->assign("articleTitle", $output_data['title']);     
        //栏目ID
        $this->assign("catid", $catid);
        
        $this->assign("titleList", $titleList);

        $this->assign("content", $output_data['content']);

        $this->commonTitle();
        
        $this->display("Show:" . $template);
    }

    //获取一级菜单
    public function commonTitle()
    {
        $category_id = M('category')->where('parentid=0')->order('catid asc')->getField('catid,arrchildid');
     
        unset($category_id['23']); //出去轮播图固定ID
        unset($category_id['17']); //出去美食中心固定ID
        unset($category_id['36']); //出去美食中心固定ID
        //没有美食中心的category
        foreach ($category_id as $k=>$v) {
            $category[]=M('category')->where("catid in (".$v.")")->field('catid, catname')->find();
        }

        foreach ($category as $key => $value) {
            $list[$value['catid']] =M('article')->where("catid={$value['catid']}")->field('catid, title,url')->select();
        }
        
        //美食中心category
        $food = M('category')->where('catid=17')->order('catid asc')->getField('catid,arrchildid');
        
        foreach ($food as $k=>$v) {
            $food_category[]=M('category')->where("catid in (".$v.")")->field('catid, catname as title,url')->select();
        }
        array_pop($food_category[0]);
        $list['17'] = $food_category['0'];
        ksort($list);
        
        $category_list = M('category')->where('parentid=0')->order('catid asc')->select();
        
        foreach ($category_list as $key => $value) {
            $categorys[$value['catid']] = $value;
        }
        
        unset($categorys['23']); //出去轮播图固定ID
        unset($categorys['36']); //出新闻动态固定ID
        
        $this->assign('lists', $list) ;
        $this->assign('titles', $categorys) ;
    }
    
    public function getArticleTitle($catid)
    {
        $titleList = M('article')->where("catid={$catid}")->field('catid, title,url')->select();
        
        return  $titleList;
    }
        
}
