<?php if (!defined('SHUIPF_VERSION')) exit(); ?>
<Admintemplate file="Common/Head"/>
<body>
<div class="wrap">
  <div id="home_toptip"></div>

  <h2 class="h_a">开发团队</h2>
  <div class="home_info" id="home_devteam">
    <ul>
      <li> <em>官网</em> <span><a href="http://www.kmeen.com" target="_blank">http://www.kmeen.com</a></span> </li>
   </ul>
  </div>
  
  <div class="table_full">

</div>
<!--升级提示-->
<div id="J_system_update" style="display:none" class="system_update"> 您正在使用旧版本的ShuipFCMS，为了获得更好的体验，请升级至最新版本。<a href="">立即升级</a> </div>
<script src="{$config_siteurl}statics/js/common.js?v"></script> 
<script>
$("#btn_submit").click(function(){
	$("#tips_success").fadeTo(500,1);
});
//获取升级信息通知
$.ajax({
    url: "{:U('Public/public_notice')}",
    dataType: "json",
    success: function (data) {
    	var r = data.data;
    	if (r.notice) {
    		$('#J_system_update').show();
    		$('#J_system_update').html(r.notice + "<a href='" + r.url +"'>立即升级</a>");
    	}
    },
    error: function () {
    }
});
</script>
</body>
</html>